# F5 Archiver Ansible Playbook

## Overview

This Ansible playbook takes a list of F5 devices from a `hosts` file located within the `inventory` directory, creates a UCS archive and copies locally into the 'tmp' direcotry.

## Requirements

This Ansible playbook requires the following:
 * ansible >= 2.5
 * python module f5-sdk
 * F5 BIG-IP running TMOS >= 12 

## Usage

Run using the `ansible-playbook` command using the inventory `-i` option to use the invertory directory instead of the default inventory host file.

**NOTE:** F5 username and password are not set in the playbook and so need to be passed into the playbook as extra variables using the `--extra-vars` option, the variables are `f5User` for the username and `f5Pwd` for the password. The below examples use the default `admin:admin`.

---
To check the playbook before using run the following commands
~~~
ansible-playbook -i inventory --extra-vars "f5User=admin f5Pwd=admin" f5Archiver.yml --syntax-check

ansible-playbook -i inventory --extra-vars "f5User=admin f5Pwd=admin" f5Archiver.yml --check
~~~

Once happy run the following to execute the playbook 

~~~
ansible-playbook -i inventory --extra-vars "f5User=admin f5Pwd=admin" f5Archiver.yml
~~~